HOME = "00073:00018:00022"
DR_CHEST = "00069:00019:00022:1"
PU_CHEST = "00069:00021:00022:1"
PORT = 00010
KEYWORD = "MySuperKey"
SIDES = {[0] = 3, 4, 2, 5}
component = setmetatable(component, {
  __index = function(_, Index) return component.proxy(component.list(Index)()) end
})
local com = component
local TRUSTED
local modem = com.modem
local DPOS
modem.open(PORT)
local drone = com.drone
local paired
Ser = {}
local StringLen, ToString, ToNumber, UnpackTable, StringRepeat, ComputerUptime, StringFormat = string.len, tostring, tonumber, table.unpack, string.rep, computer.uptime, string.format
function DecToHex(n, s) return StringFormat("%0"..s.."x", n) end
function HexToDec(s) return ToNumber("0x" .. s) end
do -- serialization
  local Types = {
    ["string"] = "0",
    ["0"] = "string",
    tostring = ToString,
    fromstring = ToString,
    ["number"] = "1",
    ["1"] = "number",
    tonumber = ToNumber,
    fromnumber = ToString,
    ["boolean"] = "2",
    ["2"] = "boolean",
    toboolean = function(n) return (n == "1" and true or false) end,
    fromboolean = function(n) return (n and "1" or "0") end,
    ["table"] = "3",
    ["3"] = "table",
    ["function"] = "0",
    fromfunction = ToString,
    ["nil"] = "0",
    fromnil = ToString
  }
  local dec2hex, hex2dec = DecToHex, HexToDec
  local function Serialize(Table)
    local SerializedTable = ""
    for Key, Value in pairs(Table) do
      local KeyType, ValueType = type(Key), type(Value)
      if Types[KeyType] and Types[ValueType] then
        local Element = ""
        local SerializedKey = Types["from"..KeyType](Key)
        Element = Element .. Types[KeyType] .. dec2hex(StringLen(SerializedKey), 2) .. SerializedKey
        local SerializedValue = Types["from"..ValueType](Value)
        Element = Element .. Types[ValueType] .. dec2hex(StringLen(SerializedValue), 4) .. SerializedValue
        SerializedTable = SerializedTable .. Element
      end
    end
    return SerializedTable
  end
  Types.fromtable = Serialize
  local function Unserialize(SerializedTable)
    if SerializedTable == "" then return {} end
    local UnserializedTable = {}
    local ElementLength, KeyHeader, KeyType, KeySize, Key, ValueHeader, ValueType, ValueSize, Value
    repeat
      ElementLength = 0
      KeyHeader = SerializedTable:sub(1, 3)
      ElementLength = ElementLength + 3
      KeyType = Types[KeyHeader:sub(1, 1)]
      KeySize = hex2dec(KeyHeader:sub(2, 3))
      Key = SerializedTable:sub(ElementLength + 1, ElementLength + KeySize)
      ElementLength = ElementLength + KeySize
      Key = Types["to" .. KeyType](Key)
      ValueHeader = SerializedTable:sub(ElementLength + 1, ElementLength + 5)
      ElementLength = ElementLength + 5
      ValueType = Types[ValueHeader:sub(1, 1)]
      ValueSize = hex2dec(ValueHeader:sub(2, 5))
      Value = SerializedTable:sub(ElementLength + 1, ElementLength + ValueSize)
      ElementLength = ElementLength + ValueSize
      Value = Types["to" .. ValueType](Value)
      UnserializedTable[Key] = Value
      SerializedTable = SerializedTable:sub(ElementLength + 1)
    until SerializedTable == ""
    return UnserializedTable
  end
  Types.totable = Unserialize
  function Ser.Do(Data)
    if type(Data) == "string" then
      return Unserialize(Data)
    else
      return Serialize(Data)
    end
  end
end
function EP()
  return computer.energy()/computer.maxEnergy()*100
end
local function energyLow()
  if EP()<10 then
    local oldpos = {UnpackTable(DPOS)}
    GotoHome()
    repeat
      Sleep(1, true)
    until EP()>95
    Goto(UnpackTable(oldpos))
  end
end
function DebugPrint(...)
  local buff = ""
  for _, v in ipairs({...}) do
    buff = buff .. ToString(v) .. " "
  end
  buff = buff .. StringRepeat(" ", 20-buff:len())
  drone.setStatusText(buff:sub(1, 10).."\n"..buff:sub(11, 20))
end
function Broadcast(...)
  modem.broadcast(PORT, Ser.Do({...}))
end
function Send(...)
  modem.send(TRUSTED, PORT, Ser.Do({...}))
end
local hooks = {}
function hook(signal, callback)
  if not hooks[signal] then hooks[signal] = {} end
  table.insert(hooks[signal], callback)
end
local PullSignal = computer.pullSignal
function Sleep(timeout, nohook)
  energyLow()
  local tm = ComputerUptime()+(timeout or 3600)
  local result
  repeat
    local event = {PullSignal(tm-ComputerUptime())}
    if not nohook then
      if event[1] then
        if hooks[event[1]] then
          for _, handler in ipairs(hooks[event[1]]) do
            result = {handler(UnpackTable(event))}
            if result[1] == false then
              break
            elseif result[1] and result[1] ~= true then
              event = result
            end
          end
        end
      end
    end
  until ComputerUptime() >= tm
end
hook("modem_message", function(_, _, remote, _, _, key)
  if key == KEYWORD then
    TRUSTED = remote
    com.eeprom.setData(remote)
    modem.send(remote, PORT, KEYWORD)
    paired = true
    return false
  end
  if remote == TRUSTED then
    return true
  else
    return false
  end
end)
local includeBuffer = ""
local function include(code)
  if code:sub(code:len()-2) == "␑" then
    includeBuffer = includeBuffer .. code:sub(0, code:len()-3)
    pcall(load(includeBuffer))
    includeBuffer = ""
  else
    includeBuffer = includeBuffer .. code
  end
end
local modes = {
  prn = DebugPrint,
  repl = function(str)
    modem.send(TRUSTED, PORT, "repl", Ser.Do({pcall(load(str))}))
  end,
  include = include
}
hook("modem_message", function(_, _, _, _, _, mode, ...)
  if modes[mode] then
    modes[mode](...)
  end
end)
local function ejectPos(PosInString)
  local PosInTable = {}
  for _, v in ipairs({PosInString:match(StringRepeat("([^:]*):?", 4))}) do
    PosInTable[#PosInTable+1] = ToNumber(v)
  end
  return PosInTable
end
DPOS = ejectPos(HOME)
function Goto(x, y, z)
  local lx,ly,lz,ofs=UnpackTable(DPOS)
  x=(x~="~")and x or lx
  y=(y~="~")and y or ly
  z=(z~="~")and z or lz
  drone.move(x-lx,y-ly,z-lz)
  repeat
    Sleep(0.1)
    ofs = drone.getOffset()
    if ofs>1 then
      if drone.getAcceleration()~=2 then
        drone.setAcceleration(2)
      end
    else
      drone.setAcceleration(0.5)
    end
  until ofs<=0.2
  DPOS={x, y, z}
end
function GotoHome()
  Goto(UnpackTable(ejectPos(HOME)))
end
local DroneSelect, DroneDrop, DroneCount, DroneSuck = drone.select, drone.drop, drone.count, drone.suck
function Drop(notreturn)
  local PosOnStart, DropChestPosition = {UnpackTable(DPOS)}, ejectPos(DR_CHEST)
  Goto(UnpackTable(DropChestPosition))
  for InternalSlot = 1, drone.inventorySize() do
    DroneSelect(InternalSlot)
    DroneDrop(SIDES[DropChestPosition[4]])
  end
  if not notreturn then
    Goto(UnpackTable(PosOnStart))
  end
end
function Pickup(notreturn)
  local PositionOnStart = {UnpackTable(DPOS)}
  local PickupChestPosition = ejectPos(PU_CHEST)
  Goto(UnpackTable(PickupChestPosition))
  local ChestEmpty = true
  local InventoryFull
  local InIterationPicked = false
  repeat
    InventoryFull = true
    ChestEmpty = true
    for InternalSlot = 1, drone.inventorySize() do
      if DroneCount(InternalSlot)==0 then
        InventoryFull = false
        break
      end
    end
    if not InventoryFull then
      if DroneSuck(SIDES[PickupChestPosition[4]]) then
        InIterationPicked = true
        ChestEmpty = false
      end
    end
  until InventoryFull or ChestEmpty
  if not notreturn then
    Goto(UnpackTable(PositionOnStart))
  end
  return InIterationPicked
end
repeat
  modem.broadcast(PORT, KEYWORD)
  Sleep(1)
until paired
repeat
  modem.send(TRUSTED, PORT, "RAI")
  Sleep(5)
until me
modem.send(TRUSTED, PORT, "log", "Ready")
repeat Sleep()until nil