local component = require("component")
local event = require("event")
local unicode = require("unicode")
local gpu = component.gpu
local RefreshFreq = 1
local TimerID
local GUI = {
  Focus = false
}
local Elements = {}
local Panels = {}
local AllObjects = {}
local DragNow
local Panel = {
  Valid = true,
  Bounds = true,
  DragDrop = false,
  ScrollEnabled = false,
  FancyBorders = false,
  EmbededTitle = false,
  FlushEnabled = false,
  DockType = false,
  Docked = {},
  DraggableLines = 1,
  Title = "",
  TitleColor = 0xffffff,
  FGC = 0xffffff,
  BGC = 0x000000,
  BorderColor = 0xbebebe,
  XPos = 1,
  YPos = 1,
  Parent = false,
  BorderW = 2,
  BorderH = 1,
  W = 10,
  H = 5,
  Index = 0,
  Children = {},
  Flush = function(self)
    if (self.FlushEnabled) then
      if self:GetParent() then
        gpu.setBackground(self:GetParent():GetBackground())
      else
        gpu.setBackground(0x000000) 
      end
      gpu.fill(self:GetPosX(), self:GetPosY(), self:GetSizeX(), self:GetSizeY(), " ")
    end
  end,
  IsScrollable = function(self)
    return self.ScrollEnabled
  end,
  EnableScroll = function(self, enable)
    self.ScrollEnabled = enable
  end,
  OnScroll = function(self, x, y, scroll)

  end,
  PostScroll = function(self, x, y, scroll)

  end,
  EnableBounds = function(self, enable)
    self.Bounds = enable
  end,
  SetBorderW = function(self, W)
    self.BorderW = W
  end,
  SetBorderH = function(self, H)
    self.BorderH = H
  end,
  PaintChildren = function(self)
    GUI.PaintChildren(self)
  end,
  GetFreeDockSpace = function(self)
    local parent = self:GetParent()
    if not parent then return 0, 0, gpu.getResolution() end
    local LeftX = #parent.Docked.LEFT > (self:Dock()=="LEFT" and 1 or 0) and (parent.Docked.LEFT[#parent.Docked.LEFT-(self:Dock()=="LEFT" and 1 or 0)]:GetLocalX() + parent.Docked.LEFT[#parent.Docked.LEFT-(self:Dock()=="LEFT" and 1 or 0)]:GetSizeX()) or 0
    local TopY = #parent.Docked.TOP > (self:Dock()=="TOP" and 1 or 0) and (parent.Docked.TOP[#parent.Docked.TOP-(self:Dock()=="TOP" and 1 or 0)]:GetLocalY() + parent.Docked.TOP[#parent.Docked.TOP-(self:Dock()=="TOP" and 1 or 0)]:GetSizeY()) or 0
    local RightX = #parent.Docked.RIGHT > (self:Dock()=="RIGHT" and 1 or 0) and (parent.Docked.RIGHT[#parent.Docked.RIGHT-(self:Dock()=="RIGHT" and 1 or 0)]:GetLocalX()) or parent:GetSizeX() - self:GetParentBorderW() * 2
    local BottomY = #parent.Docked.BOTTOM > (self:Dock()=="BOTTOM" and 1 or 0) and (parent.Docked.BOTTOM[#parent.Docked.BOTTOM-(self:Dock()=="BOTTOM" and 1 or 0)]:GetLocalY()) or parent:GetSizeY() - self:GetParentBorderH() * 2
    return LeftX, TopY, RightX, BottomY
  end,
  DockChanged = function(self, docktype)
    local lx, ty, rx, by = self:GetFreeDockSpace()
    if (docktype=="FILL") then
      self.XPos = lx
      self.YPos = ty
      self.W = rx - lx
      self.H = by - ty
    elseif (docktype=="LEFT") then
      self.XPos = lx
      self.YPos = 0
      self.H = by
    elseif (docktype=="RIGHT") then
      self.XPos = rx - self:GetSizeX()
      self.YPos = ty
      self.H = by
    elseif (docktype=="TOP") then
      self.XPos = 0
      self.W = self:GetParent():GetSizeX() - self:GetParentBorderW()*2
      self.YPos = ty
    elseif (docktype=="BOTTOM") then
      self.XPos = 0
      self.W = self:GetParent():GetSizeX() - self:GetParentBorderW()*2
      self.YPos = by - self:GetSizeY()
    elseif (docktype=="CENTER") then
      self.XPos = self:GetParentPosX() + self:GetParent():GetSizeX()/2 - self:GetSizeX()
      self.YPos = self:GetParentPosY() + self:GetParent():GetSizeY()/2 - self:GetSizeY()
    end
  end,
  CallDocked = function(self)
    if not self:IsValid() then return end
    self:DockChanged(self:Dock())
    for _, DockType in pairs(self.Docked) do
      for _, DockedObject in pairs(DockType) do
        DockedObject:CallDocked()
      end
    end
  end,
  Dock = function(self, docktype)
    if not docktype then return self.DockType or "NODOCK" end
    if not self:GetParent() then return end
    for key, obj in pairs(self:GetParent().Docked[self:Dock()]) do
      if obj == self then
        table.remove(self:GetParent().Docked[self:Dock()], key)
      end
    end
    docktype = string.upper(docktype)
    self.DockType = docktype
    local parent = self:GetParent()
    table.insert(parent.Docked[docktype], self)
    if self:GetParent() then self:GetParent():CallDocked() else self:CallDocked() end
  end,
  IsValid = function(self)
    return self.Valid
  end,
  GetParent = function(self)
    return self.Parent
  end,
  IsChild = function(self)
    return self:GetParent() and true or false
  end,
  IsParent = function(self)
    return #self:GetChildren()>0
  end,
  EnableFlush = function(self, enable)
    self.FlushEnabled = enable
  end,
  IsFlushEnabled = function(self)
    return self.FlushEnabled
  end,
  GetForeground = function(self)
    return self.FGC
  end,
  GetBackground = function(self)
    return self.BGC
  end,
  GetBorderColor = function(self)
    return self.BorderColor
  end,
  SetForeground = function(self, color)
    if type(color)=="table" then
      color = GUI.rgb2dec(table.unpack(color))
    end
    self.FGC = color
  end,
  SetBackground = function(self, color)
    if type(color)=="table" then
      color = GUI.rgb2dec(table.unpack(color))
    end
    self.BGC = color
  end,
  SetBorderColor = function(self, color)
    if type(color)=="table" then
      color = GUI.rgb2dec(table.unpack(color))
    end
    self.BorderColor = color
  end,
  SetColor = function(self, color)
    self:SetForeground(color)
    self:SetBackground(color)
  end,
  GetParentPosX = function(self)
    return self.Parent and self.Parent:GetPosX() or 0
  end,
  GetParentPosY = function(self)
    return self.Parent and self.Parent:GetPosY() or 0
  end,
  GetParentBorderW = function(self)
    return self.Parent and (self.Parent:IsBorderEnabled() and self.Parent.BorderW or 0) or 0
  end,
  GetParentBorderH = function(self)
    return self.Parent and (self.Parent:IsBorderEnabled() and self.Parent.BorderH or 0) or 0
  end,
  GetPosX = function(self)
    return self:GetParentPosX() + self:GetParentBorderW() + self.XPos
  end,
  GetLocalX = function(self)
    return self.XPos
  end,
  GetLocalY = function(self)
    return self.YPos
  end,
  GetPosY = function(self)
    return self:GetParentPosY() + self:GetParentBorderH() + self.YPos
  end,
  GetPos = function(self)
    return self:GetPosX(), self:GetPosY()
  end,
  GetLocalPos = function(self)
    return self:GetLocalX(), self:GetLocalY()
  end,
  GetSize = function(self)
    return self.W, self.H
  end,
  GetSizeX = function(self)
    return self.W
  end,
  GetMaxSizeX = function(self)
    local maxwidth = gpu.getResolution()
    local parentwidth = self:GetParent() and (self:GetParent():GetSizeX() - (self:GetParent().Bounds and self:GetParentBorderW()*2 or 0)) or maxwidth
    return self:GetParent() and parentwidth - self:GetLocalX() or parentwidth
  end,
  GetFreeSpace = function(self)

  end,
  GetSizeY = function(self)
    return self.H
  end,
  GetMaxSizeY = function(self)
    local _, maxheight = gpu.getResolution()
    local parentheight = self:GetParent() and (self:GetParent():GetSizeY() - (self:GetParent().Bounds and self:GetParentBorderH()*2 or 0)) or maxheight
    return self:GetParent() and parentheight - self:GetLocalY() or parentheight
  end,
  SetSizeY = function(self, y)
    y = (y<self:GetMaxSizeY()) and y or self:GetMaxSizeY()
    self.H = y or self:GetSizeY()
    if self:GetParent() then self:GetParent():CallDocked() else self:CallDocked() end
  end,
  SetSizeX = function(self, x)
    x = (x<self:GetMaxSizeX()) and x or self:GetMaxSizeX()
    self.W = x or self:GetSizeX()
    if self:GetParent() then self:GetParent():CallDocked() else self:CallDocked() end
  end,
  SetSize = function(self, x, y)
    self:SetSizeX(x)
    self:SetSizeY(y)
  end,
  SetPos = function(self, x, y)
    if not y then y = self:GetPosY() end
    if not x then x = self:GetPosX() end
    self:Flush()
    self.XPos = self.Bounds and (x >= 0 and x or 0) or x
    self.YPos = self.Bounds and (y >= 0 and y or 0) or y
    self:Paint(self:GetSize())
  end,
  GetTitle = function(self)
    return self.Title
  end,
  SetTitle = function(self, title)
    self.Title = title
  end,
  GetTitleColor = function(self)
    return self.TitleColor
  end,
  SetTitleColor = function(self, color)
    if type(color)=="table" then
      color = GUI.rgb2dec(table.unpack(color))
    end
    self.TitleColor = color
  end,
  Initialize = function(self)

  end,
  DoClick = function(self, x, y, btn, ply)

  end,
  OnDrag = function(self, x, y)
    x = x-self:GetParentPosX()
    y = y-self:GetParentPosY()
    self:SetPos(x, y)
  end,
  PostDrag = function(self, x, y)

  end,
  OnPickup = function(self, x, y)
    self.OnDragBorderColor = self:GetBorderColor()
    self:SetBorderColor({255, 255, 255})
    self:Paint(self:GetSize())
  end,
  PostPickup = function(self, x, y)

  end,
  OnDrop = function(self, x, y)
    self:SetBorderColor(self.OnDragBorderColor)
    if (self:GetParent()) then
      self:GetParent():PaintChildren()
    else
      self:PaintChildren()
    end
  end,
  PostDrop = function(self, x, y)

  end,
  IsDraggable = function(self)
    return self.DragDrop
  end,
  EnableDragDrop = function(self, enable)
    self.DragDrop = enable
  end,
  GetDragLines = function(self)
    return self.DraggableLines
  end,
  SetDragLines = function(self, lines)
    self.DraggableLines = lines < self:GetSizeY() and lines or self:GetSizeY()
  end,
  EnableBorder = function(self, enable)
    self.FancyBorders = enable
  end,
  IsBorderEnabled = function(self)
    return self.FancyBorders
  end,
  EnableTitle = function(self, enable)
    self.EmbededTitle = enable
  end,
  IsTitleEnabled = function(self)
    return self.EmbededTitle
  end,
  Paint = function(self, W, H)
    if not self.Valid then return false end
    self:Flush()
    W = W or self:GetSizeX()
    H = H or self:GetSizeY()
    if (self.FancyBorders) then
      gpu.setBackground(self:GetBorderColor())
      gpu.fill(self:GetPosX(), self:GetPosY(), W, H, " ")
    end
    if (self.EmbededTitle) then
      gpu.setForeground(self:GetTitleColor())
      gpu.set(self:GetPosX()+1, self:GetPosY(), self:GetTitle())
    end
    gpu.setForeground(self:GetForeground())
    gpu.setBackground(self:GetBackground())
    if (self.FancyBorders) then
      gpu.fill(self:GetPosX()+self.BorderW, self:GetPosY()+self.BorderH, W-self.BorderW*2, H-self.BorderH*2, " ")
    else
      gpu.fill(self:GetPosX(), self:GetPosY(), W, H, " ")
    end
  end,
  PaintOver = function() end,
  Remove = function(self)
    self:Flush()
    self.Valid = false
    for _, child in pairs(self:GetChildren()) do
      child:Remove()
    end
  end,
  Add = function(self, child)
    if type(child) == "string" then
      child = GUI.Create(child, true)
    end
    child.Parent = self
    table.insert(self.Children, child)
    return child
  end,
  GetChildren = function(self)
    return self.Children
  end
}

local Frame = setmetatable({
  FancyBorders = true,
  FlushEnabled = true,
  EmbededTitle = true,
  Title = "SFrame",
  DragDrop = true,
  W = 35,
  H = 10,
  Initialize = function(frame)
    local CloseButton = frame:Add("button")
    CloseButton:SetLabel("X")
    CloseButton:EnableBounds(false)
    CloseButton:SetSize(1, 1)
    CloseButton:SetBackground({250, 30, 30})
    function CloseButton.DoClick(self)
      frame:Remove()
    end
    function frame.PaintOver(self, W, H)
      CloseButton:SetPos(W-3, -1)
    end
  end
}, {__index = Panel})

local Text = setmetatable({
  Value = "slabel",
  ParentColors = true,
  SetValue = function(self, value)
    self:Flush()
    self:SetSize(unicode.len(value), 1)
    self.Value = tostring(value)
    self:Paint(self:GetSize())
  end,
  GetBackground = function(self)
    if (self.ParentColors and self:GetParent()) then
      return self:GetParent():GetBackground()
    else
      return self.BGC
    end
  end,
  GetForeground = function(self)
    if (self.ParentColors and self:GetParent()) then
      return self:GetParent():GetForeground()
    else
      return self.FGC
    end
  end,
  EnableParentColors = function(self, enable)
    self.ParentColors = enable
  end,
  GetValue = function(self)
    return self.Value
  end,
  Paint = function(self, W, H)
    gpu.setForeground(self:GetForeground())
    gpu.setBackground(self:GetBackground())
    self:Flush()
    gpu.set(self:GetPosX(), self:GetPosY(), self:GetValue())
  end
}, {__index = Panel})

local RichText = setmetatable({
  Lines = {},
  ScrollEnabled = true,
  SelectedLine = 1,
  AppendLine = function(self, str)
    if #self.Lines == self.SelectedLine then
      self.SelectedLine = self.SelectedLine + 1
    end
    table.insert(self.Lines, str)
    self:PaintChildren()
  end,
  Clear = function(self)
    self.Lines = {}
    self.SelectedLine = 1
  end,
  AppendText = function(self, str)
    local IncreaseLine
    local SubSize = self:GetSizeX()
    if self.SelectedLine >= #self.Lines-5 then
      IncreaseLine = true
    end
    local i = 1
    repeat
      i = i + 1
      local substring = unicode.sub(str, 0, SubSize-1)
      for substr in substring:gmatch("([^\n]*)\n?") do
        if substr:find("%S") then
          table.insert(self.Lines, substr:match("%s*(.*)"))
          if (IncreaseLine) then
            self.SelectedLine = self.SelectedLine + 1
          end
        end
      end
      str = unicode.sub(str, SubSize)
    until str==""
    self:PaintChildren()
  end,
  OnScroll = function(self, _, _, scroll)
    local newLine = self.SelectedLine - scroll
    self.SelectedLine = newLine>0 and (newLine<#self.Lines and newLine or #self.Lines) or 1
    self:PaintChildren()
  end,
  Paint = function(self, W, H)
    if not self.Valid then return end
    if not W then W = self:GetSizeX() end
    if not H then H = self:GetSizeY() end
    gpu.setBackground(self:GetBackground())
    gpu.setForeground(self:GetForeground())
    local X, Y = self:GetPos()
    gpu.fill(X, Y, W, H, " ")
    local YP = 0
    local From = self.SelectedLine - (H<=#self.Lines and H or #self.Lines) + 1
    local To = self.SelectedLine
    --local ScrollerSize = math.ceil(H/(#self.Lines/(To-From)))
    --ScrollerSize = ScrollerSize>H and ScrollerSize or H
    --local ScrollerPos = self:GetPosY()+0
    --gpu.fill(self:GetPosX()+self:GetSizeX()-1, ScrollerPos, 1, ScrollerSize, "█")
    if (To - From < H) then
      YP = YP + (H - (To - From)) - 1
    end
    for l = From, To do
      if (self.Lines[l]) then
        gpu.set(X, Y+YP, self.Lines[l])
      end
      YP = YP + 1
    end
  end
}, {__index = Panel})

local Button = setmetatable({
  Active = false,
  Sticky = false,
  PressTime = 2,
  Label = "SButton",
  W = 10,
  H = 3,
  BGC = 0x222222,
  SetLabel = function(self, label)
    self.Label = label
  end,
  IsSticky = function(self)
    return self.Sticky
  end,
  SetSticky = function(self, enable)
    self.Sticky = enable
  end,
  GetLabel = function(self)
    return self.Label
  end,
  IsActive = function(self)
    return self.Active
  end,
  Callback = function(self, active) end,
  SetCallback = function(self, callback)
    self.Callback = callback
  end,
  DoClick = function(self)
    self.Active = not self.Active
    if not self.Active and self.TimerID then
      event.cancel(self.TimerID)
    end
    if not self:IsSticky() and self.Active then
      self.TimerID = event.timer(self.PressTime, function()
        if not self.Valid or (not self.Active) then return end
        self.Active = false
        self:Paint(self:GetSize())
        self:Callback(self.Active)
      end)
    end
    self:Callback(self.Active)
    self:Paint(self:GetSize())
  end,
  Paint = function(self, W, H)
    if not self.Valid then return end
    local bgc
    if (self:IsActive()) then
      local h, s, v = GUI.dec2hsv(self:GetBackground())
      bgc = GUI.hsv2dec(h, s-0.2, v-0.2)
    else
      bgc = self:GetBackground()
    end
    gpu.setBackground(bgc)
    gpu.setForeground(self:GetForeground())
    gpu.fill(self:GetPosX(), self:GetPosY(), W, H, " ")
    gpu.set(math.floor(self:GetPosX() + self:GetSizeX()/2 - unicode.len(self:GetLabel())/2), math.floor(self:GetPosY() + self:GetSizeY()/2), self:GetLabel())
  end
}, {__index = Panel})

local TextEntry = setmetatable({
  Valid = true,
  Value = "",
  Cursor = 0,
  CursorBlinkColor = 0xffffff,
  Mask = "",
  H = 1,
  W = 15,
  BGC = 0x000000,
  FGC = 0xffffff,
  History = {
    Pos = 0
  },
  Paint = function(self, W, H)
    if not self.Valid then return false end
    W = W or self:GetSizeX()
    H = H or self:GetSizeY()
    gpu.setForeground(self:GetForeground())
    gpu.setBackground(self:GetBackground())
    gpu.fill(self:GetPosX(), self:GetPosY(), W, H, " ")
    local lp = unicode.len(self.Value)-W+1
    lp = lp>0 and lp or 0
    gpu.set(self:GetPosX(), self:GetPosY(), unicode.sub(self.Value, lp, self.Cursor))
  end,
  OnEnter = function(self, value)

  end,
  DoClick = function(self)
    local keycode
    local char
    self.History.Pos = #self.History+1
    repeat
      _, _, keycode, keycode2 = event.pull("key_down")
      if GUI.Focus == self then
        char = unicode.char(keycode)
        if not char:match("%c") then
          self.Value = self.Value .. char
          self.Cursor = self.Cursor + 1
        else
          if keycode == 8 then --Backspace
            self.Value = unicode.sub(self.Value, 0, self.Cursor-1)..unicode.sub(self.Value, self.Cursor+1)
            self.Cursor = self.Cursor - (self.Cursor>0 and 1 or 0)
          elseif keycode == 13 then --Enter
            self.History[#self.History+1] = self.Value
            self.History.Pos = #self.History+1
            self:OnEnter(self.Value)
            self.Value = ""
            self.Cursor = 0
          elseif keycode == 4 then
            break
          elseif keycode == 0 then
            if keycode2 == 203 then --LeftArrow
              self.Cursor = self.Cursor - (self.Cursor>0 and 1 or 0)
            elseif keycode2 == 205 then --RightArrow
              self.Cursor = self.Cursor + (self.Cursor<unicode.len(self.Value) and 1 or 0)
            elseif keycode2 == 200 then --UpArrow
              if self.History.Pos>#self.History then
                self.History.Temp = self.Value
              end
              self.History.Pos = self.History.Pos>1 and self.History.Pos - 1 or self.History.Pos
              self.Value = self.History[self.History.Pos]
              self.Cursor = unicode.len(self.Value)
            elseif keycode2 == 208 then --DownArrow
              self.History.Pos = self.History.Pos<#self.History+1 and self.History.Pos + 1 or self.History.Pos
              if self.History.Pos>#self.History then
                self.Value = self.History.Temp
              else
                self.Value = self.History[self.History.Pos]
              end
              self.Cursor = unicode.len(self.Value)
            elseif keycode2 == 207 then --END
              self.Cursor = unicode.len(self.Value)
            end
          end
        end
        self:Paint()
      end
    until GUI.Focus ~= self
  end
}, {__index = Panel})

function GUI.Register(elementname, index, description)
  Elements[elementname] = {index = index, description = function() return description end}
end
function GUI.Create(elementname, isChild)
  if not Elements[elementname] then
    return false
  end
  local obj = setmetatable({
    Index=#Panels+1, 
    IsChild = isChild, 
    Children = {},
    Lines = {},
    Docked = {
      LEFT = {},
      RIGHT = {},
      TOP = {},
      BOTTOM = {},
      CENTER = {},
      FILL = {},
      NODOCK = {}
    },
  }, {
    __index = Elements[elementname].index,
    __tostring = Elements[elementname].description,
    __call = function(self) return getmetatable(self) end
  })
  if not isChild then
    table.insert(Panels, obj)
  else
    obj:SetPos(0, 0)
  end
  table.insert(AllObjects, obj)
  obj:Initialize()
  return obj
end
function GUI.ElementsList()
  return Elements
end
function GUI.rgb2hex(r, g, b)
  return string.format("0x%02x%02x%02x", r, g, b)
end
function GUI.rgb2dec(r, g, b)
  return tonumber(GUI.rgb2hex(r, g, b))
end
function GUI.hex2rgb(hex)
  hex = hex:gsub("0x","")
  return tonumber("0x"..hex:sub(1,2)), tonumber("0x"..hex:sub(3,4)), tonumber("0x"..hex:sub(5,6))
end
function GUI.rgb2hsv(r, g, b)
  r, g, b = r / 255, g / 255, b / 255
  local max, min = math.max(r, g, b), math.min(r, g, b)
  local h, s, v
  v = max
  local d = max - min
  if max == 0 then
    s = 0
  else
    s = d / max
  end
  if max == min then
    h = 0
  else
    if max == r then
      h = (g - b) / d
    if g < b then
      h = h + 6
    end
    elseif max == g then
      h = (b - r) / d + 2
    elseif max == b then
      h = (r - g) / d + 4
    end
    h = h / 6
  end
  return h, s, v
end
function GUI.hsv2rgb(h, s, v)
  local r, g, b
  local i = math.floor(h * 6);
  local f = h * 6 - i;
  local p = v * (1 - s);
  local q = v * (1 - f * s);
  local t = v * (1 - (1 - f) * s);
  i = i % 6
  if i == 0 then r, g, b = v, t, p
  elseif i == 1 then r, g, b = q, v, p
  elseif i == 2 then r, g, b = p, v, t
  elseif i == 3 then r, g, b = p, q, v
  elseif i == 4 then r, g, b = t, p, v
  elseif i == 5 then r, g, b = v, p, q
  end
  return r * 255, g * 255, b * 255
end
function GUI.dec2hex(Dec)
  return string.format("0x%06x", Dec)
end
function GUI.dec2rgb(dec)
  local hex = string.format("%06x", dec)
  return tonumber("0x"..hex:sub(1, 2)), tonumber("0x"..hex:sub(3, 4)), tonumber("0x"..hex:sub(5, 6))
end
function GUI.dec2hsv(dec)
  return GUI.rgb2hsv(GUI.dec2rgb(dec))
end
function GUI.hsv2dec(h, s, v)
  return GUI.rgb2dec(GUI.hsv2rgb(h, s, v))
end
event.listen("touch", function(_, _, x, y, btn, ply)
  for _, panel in pairs(AllObjects) do
    local px, py = panel:GetPos()
    local pw, ph = panel:GetSize()
    if (x>=px) and (y>=py) then
      if (x<px+pw) and (y<py+ph) then
        if panel:IsValid() then
          GUI.Focus = panel
          panel:DoClick(x, y, btn, ply)
        end
      end
    end
  end
end)
event.listen("scroll", function(_, _, x, y, scroll)
  for _, panel in pairs(AllObjects) do
    if panel:IsScrollable() then
      local px, py = panel:GetPos()
      local pw, ph = panel:GetSize()
      if (x>=px) and (y>=py) then
        if (x<px+pw) and (y<py+ph) then
          if panel:IsValid() then
            panel:OnScroll(x, y, scroll)
            panel:PostScroll(x, y, scroll)
          end
        end
      end
    end
  end
end)
event.listen("drag", function(_, _, x, y, btn, ply)
  if not DragNow then
    for _, panel in pairs(AllObjects) do
      if panel:IsDraggable() then
        local px, py = panel:GetPos()
        local pw, ph = panel:GetSize()
        if (x>=px) and (y>=py) then
          if (x<px+pw) and (y<py+panel:GetDragLines()) then
            if panel:IsValid() then
              DragNow = panel
              panel:OnPickup(x, y, btn, ply)
              panel:PostPickup(x, y, btn, ply)
            end
          end
        end
      end
    end
  else
    DragNow:OnDrag(x, y, btn, ply)
    DragNow:PostDrag(x, y, btn, ply)
  end
end)
event.listen("drop", function(_, _, x, y, btn, ply)
  if not DragNow then return end
  DragNow:OnDrop(x, y, btn, ply)
  DragNow:PostDrop(x, y, btn, ply)
  DragNow = false
end)
function GUI.PaintChildren(parent)
  if not parent:IsValid() then return end
  local obgc, ofgc = gpu.getBackground(), gpu.getForeground()
  parent:Paint(parent:GetSize())
  parent:PaintOver(parent:GetSize())
  gpu.setBackground(obgc)
  gpu.setForeground(ofgc)
  if not parent:IsParent() then return end
  for _, child in pairs(parent:GetChildren()) do
    if not child.Valid then return end
    GUI.PaintChildren(child)
  end
end
function GUI.RefreshFrequency(newfreq)
  if TimerID then event.cancel(TimerID) end
  TimerID = event.timer(newfreq, function()
    for _, panel in pairs(Panels) do
      GUI.PaintChildren(panel)
    end
  end, math.huge)
end
function GUI.Cleanup()
  for _, obj in pairs(Panels) do
    obj:Remove()
  end
end


GUI.RefreshFrequency(RefreshFreq)
GUI.Register("panel", Panel, "Простейший элемент agui. Основа большинства элементов.")
GUI.Register("frame", Frame, "Простое окно на основе spanel.")
GUI.Register("label", Text, "Простой текст.")
GUI.Register("button", Button, "Простая кнопка.")
GUI.Register("text", RichText, "Текст с поддержкой скроллинга.")
GUI.Register("textentry", TextEntry, "Текст с поддержкой скроллинга.")

return GUI