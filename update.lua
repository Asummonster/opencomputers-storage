local component = require("component")
local fs = require("filesystem")
local internet = component.internet
local function download(path, url)
    local file = io.open(path, "w")
    local stream = internet.request(url, _, {
        ["user-agent"]="Mozilla/5.0"
    })
    local chunk
    repeat
        chunk = stream.read(4096)
        if chunk then
            file:write(chunk)
        end
    until not chunk
    stream:close()
    file:flush()
    file:close()
end
local downloadlist = {
    {"/lib/agui.lua", "https://gitlab.com/Asummonster/opencomputers-storage/-/raw/master/agui.lua"},
    {"/lib/emser.lua", "https://gitlab.com/Asummonster/opencomputers-storage/-/raw/master/emser.lua"},
    {"drone.lua", "https://gitlab.com/Asummonster/opencomputers-storage/-/raw/master/OCSDrone/drone.min.lua"},
    {"me.lua", "https://gitlab.com/Asummonster/opencomputers-storage/-/raw/master/me.lua"},
    {"ocstore.lua", "https://gitlab.com/Asummonster/opencomputers-storage/-/raw/master/ocstore.lua"},
    {"reinstall_ocstore.lua", "https://gitlab.com/Asummonster/opencomputers-storage/-/raw/master/update.lua"}
}
for _, downloaddata in ipairs(downloadlist) do
    download(table.unpack(downloaddata))
end