local PORT = 10
local DRONEMODEM
local KEYWORD = "MySuperKey"
_G.DroneInclusions = {}
local event = require("event")
local component = require("component")
local unicode = require("unicode")
local gui = require("agui")
gui.RefreshFrequency(math.huge)
local emser = require("emser")
local serialization = require("serialization")
local gpu = component.gpu
local modem = component.modem
local MaxPacketSize = modem.maxPacketSize()
local ResolutionX, ResolutionY = gpu.getResolution()
local Store = {}
local StoreIndexes = {
  indexes = {},
  names = {}
}
local StoreRaw = {}
local Cart = {}
modem.open(PORT)
do --PAIR
  repeat
    modem.broadcast(PORT, KEYWORD)
    local _, _, address, _, _, key = event.pull("modem_message")
    print(address, key)
    if key==KEYWORD then
      DRONEMODEM = address
    end
  until DRONEMODEM
end
local function LoadStoreFromFile()
  local stream = io.open("/etc/.OCStore", "r")
  local chunk
  local buff = ""
  repeat
    chunk = stream:read()
    if chunk then
      buff = buff .. chunk
    end
  until not chunk
  stream:close()
  return emser.Do(buff)
end
local function SaveStoreToFile(Tbl)
  local buff = emser.Do(Tbl)
  local stream = io.open("/etc/.OCStore", "w")
  repeat
    stream:write(buff:sub(0, 1024))
    buff = buff:sub(1025)
  until buff == ""
  stream:close()
end
if pcall(io.lines, "/etc/.OCStore") then
  StoreRaw = LoadStoreFromFile()
end
local function UpdateStore()
  StoreIndexes = {
    indexes = {},
    names = {}
  }
  Store = {}
  local IndexCounter = 1
  for cell, cellinfo in pairs(StoreRaw) do
    for item, count in pairs(cellinfo) do
      if not Store[item] then
        Store[item] = {}
        table.insert(StoreIndexes.indexes, IndexCounter)
        IndexCounter = IndexCounter + 1
        table.insert(StoreIndexes.names, item)
      end
      if not Store[item][cell] then Store[item][cell] = 0 end
      Store[item][cell] = Store[item][cell] + count
    end
  end
end
do --Arguments processing
  local function parseArgs(Args)
    local Parsed = {}
    for key, value in string.gmatch(Args, "-?(%w)%W*(%w*)") do
      Parsed[key]=value
    end
    return Parsed
  end
  local Args = parseArgs(table.concat({...}, ""))
  if Args.x then ResolutionX = tonumber(Args.x) end
  if Args.y then ResolutionY = tonumber(Args.y) end
end
local function Include(Path, DroneRequestedIncluded)
  local Buffer = ""
  local Chunk
  if not DroneRequestedIncluded then
    local file = io.open(Path)
    if not file then return false, "file not found" end
    repeat
      Chunk = file:read(2048)
      Buffer = Buffer .. (Chunk or "")
    until not Chunk
    _G.DroneInclusions[Path] = Buffer
  else
    Buffer = _G.DroneInclusions[Path]
  end
  repeat
    Chunk = Buffer:sub(1, MaxPacketSize-20)
    modem.send(DRONEMODEM, PORT, "include", Chunk)
    Buffer = Buffer:sub(MaxPacketSize-19)
  until Buffer == ""
  modem.send(DRONEMODEM, PORT, "include", "␑")
  return true, Path.." loaded!"
end
local function ReInclude()
  for path in pairs(_G.DroneInclusions) do
    print(Include(path, true))
  end
end
if not _G.DroneRAIHandler then
  event.listen("modem_message", function(_, _, _, _, _, Message)if Message=="RAI" then ReInclude()end end)
  _G.DroneRAIHandler = true
end
local LogAppendText
do --Inclusions
  Include("me.lua")
end
local OriginalResolution = {gpu.getResolution()}
do --Interface
  gpu.setResolution(ResolutionX, ResolutionY)
  local Screenspace = gui.Create("panel")
  Screenspace:SetSize(ResolutionX, ResolutionY)
  local Repl, StoreRichText, CartRichText
  do --Log&REPL
    local LogPanel = Screenspace:Add("panel")
    LogPanel:SetSize(math.ceil(ResolutionX/3), 25)
    LogPanel:Dock("RIGHT")
    Repl = LogPanel:Add("textentry")
    Repl:Dock("BOTTOM")
    function Repl.OnEnter(_, text)
      LogAppendText("<<"..text)
      local result_ret = load("return "..text)
      if result_ret then
        modem.send(DRONEMODEM, PORT, "repl", "return "..text)
      else
        local result, err = load(text)
        if result then
          modem.send(DRONEMODEM, PORT, "repl", text)
        else
          LogAppendText("Syntax error!")
          LogAppendText(err)
        end
      end
    end
    local LogText = LogPanel:Add("text")
    LogText:SetBackground(0xffffff)
    LogText:SetForeground(0x000000)
    LogText:Dock("FILL")
    LogAppendText = function(str)
      LogText:AppendText(str)
    end
    LogText:AppendLine("OCStore Logs&REPL")
    event.listen("modem_message", function(_, _, _, _, _, mode, message)
      if mode == "repl" then
        local result = emser.Do(message)
        if result[1]==true then table.remove(result, 1) end
        for _, v in ipairs(result) do
          if type(v)=="table" then
            LogAppendText(">>"..serialization.serialize(v, true))
          else
            LogAppendText(">>"..tostring(v))
          end
        end
      elseif mode == "log" then
        LogAppendText("[LOG]"..message)
      end
    end)
  end
  do --Store&Cart
    local function AvailableOnStore(itemname)
      if not Store[itemname] then return 0 end
      local count = 0
      for _, refcount in pairs(Store[itemname]) do
        count = count + refcount
      end
      if Cart[itemname] then
        count = count - Cart[itemname].CART
      end
      return count
    end
    local function ModifyRichText(RichTextObj, TBL)
      RichTextObj.Lines = {"Store"}
        local count, index
        for item, references in pairs(TBL) do
          if references.CART then
            count = references.CART
          else
            count = AvailableOnStore(item)
          end
          for id, name in ipairs(StoreIndexes.names) do
            if name == item then
              index = id
              break
            end
          end
          table.insert(RichTextObj.Lines, "<" .. index .. "> " .. item:match("([^~]*)~").." : "..count)
        end
        RichTextObj.SelectedLine = #RichTextObj.Lines
        RichTextObj:PaintChildren()
    end
    local MainPanel = Screenspace:Add("panel")
    MainPanel:Dock("FILL")
    local BottomPanel = MainPanel:Add("panel")
    BottomPanel:SetSize(0, 9)
    BottomPanel:Dock("BOTTOM")
    local Entry = BottomPanel:Add("textentry")
    Entry:Dock("TOP")
    CartRichText = BottomPanel:Add("text")
    CartRichText:Dock("FILL")
    CartRichText:SetBackground(0x545454)
    StoreRichText = MainPanel:Add("text")
    StoreRichText:Dock("FILL")
    StoreRichText:SetBackground(0x282828)
    local commands
    event.listen("modem_message", function(_, _, _, _, _, message)
      local status, unpacked = pcall(emser.Do, message)
      if not status then return end
      if unpacked[1]=="__reindex" then
        StoreRaw[unpacked[2][1]] = unpacked[2][2]
        LogAppendText("[Reindexed] "..unpacked[2][1])
      elseif unpacked[1]=="__reindex_finished" then
        commands.update()
      end
    end)
    local lastsearch = ""
    commands = {
      find = function(pattern)
        lastsearch = pattern
        local FindResults = {}
        for Item, References in pairs(Store) do
          if unicode.lower(Item):find(unicode.lower(pattern)) then
            FindResults[Item]=References
          end
        end
        ModifyRichText(StoreRichText, FindResults)
      end,
      add = function(itemindex, RequestedCount)
        itemindex = tonumber(itemindex)
        RequestedCount = tonumber(RequestedCount)
        if StoreIndexes.names[itemindex] then
          local itemname = StoreIndexes.names[itemindex]
          local Count = AvailableOnStore(itemname)
          if RequestedCount > Count then
            LogAppendText(string.format("Requested %s, available %s", RequestedCount, Count))
            RequestedCount = Count
          end
          if not Cart[itemname] then Cart[itemname] = {CART = 0} end
          Cart[itemname].CART = Cart[itemname].CART + RequestedCount
          ModifyRichText(CartRichText, Cart)
          commands.find(lastsearch)
        else
          LogAppendText(string.format("Item <%s> not found.", itemindex))
        end
      end,
      remove = function(itemindex, RequestedCount)
        itemindex = tonumber(itemindex)
        RequestedCount = tonumber(RequestedCount)
        if StoreIndexes.names[itemindex] then
          local itemname = StoreIndexes.names[itemindex]
          if Cart[itemname].CART then
            Cart[itemname].CART = Cart[itemname].CART - RequestedCount
            if Cart[itemname].CART < 1 then
              Cart[itemname] = nil
            end
            ModifyRichText(CartRichText, Cart)
            commands.find(lastsearch)
          end
        else
          LogAppendText(string.format("Item <%s> not found.", itemindex))
        end
      end,
      sort = function()
        modem.send(DRONEMODEM, PORT, "repl", "me.Sort()")
      end,
      reindex = function(Cluster, Sector, Block)
        modem.send(DRONEMODEM, PORT, "repl", string.format("me.ReindexStore(%s, %s, %s)", Cluster == "" and "nil" or Cluster, Sector == "" and "nil" or Sector, Block == "" and "nil" or Block))
      end,
      update = function()
        SaveStoreToFile(StoreRaw)
        UpdateStore()
        ModifyRichText(StoreRichText, Store)
      end,
      help = function()
        LogAppendText("help\nreindex [Cluster [Sector [Block]]]\nsort\nupdate\nfind something\nadd index count\nremove index count")
      end,
      request = function()
        local request = {}
        local Cluster, Sector, Block, Side
        local RequestedCount = 0
        for itemname, refs in pairs(Cart) do
          RequestedCount = refs.CART
          for reference, count in pairs(Store[itemname]) do
            Cluster, Sector, Block, Side = reference:match("(%d*):(%d*):(%d*):(%d*)")
            Cluster, Sector, Block, Side = tonumber(Cluster), tonumber(Sector), tonumber(Block), tonumber(Side)
            if count >= RequestedCount then
              table.insert(request, {
                Cluster = Cluster,
                Sector = Sector,
                Block = Block,
                Side = Side,
                Id = itemname,
                Count = RequestedCount
              })
              StoreRaw[reference][itemname] = StoreRaw[reference][itemname] - RequestedCount
              break
            else
              table.insert(request, {
                Cluster = Cluster,
                Sector = Sector,
                Block = Block,
                Side = Side,
                Id = itemname,
                Count = count
              })
              StoreRaw[reference][itemname] = nil
              RequestedCount = RequestedCount - count
            end
          end
        end
        modem.send(DRONEMODEM, PORT, "repl", string.format("me.ProcessRequest(\"%s\")", emser.Do(request)))
        Cart = {}
        ModifyRichText(CartRichText, Cart)
        commands.update()
      end,
      exit = function()
        gui.Focus = false
        event.push("key_down")
        event.push("interrupted")
      end
    }
    commands.update()
    local command, arg1, arg2, arg3
    Entry.OnEnter = function(_, text)
      command, arg1, arg2, arg3 = text:match("(%S*)%s*(%S*)%s*(%S*)%s*(%S*)%s*")
      if commands[command] then
        LogAppendText(string.format("[%s]%s %s %s", command, arg1, arg2, arg3))
        commands[command](arg1, arg2, arg3)
      else
        LogAppendText(string.format("Wrong command \"%s\"", command))
      end
    end
    Screenspace:PaintChildren()
    gui.Focus = Entry
    Entry:DoClick()
    event.pull("interrupted")
  end
  Screenspace:Remove()
  gpu.fill(1, 1, ResolutionX, ResolutionY, " ")
end
gpu.setResolution(table.unpack(OriginalResolution))