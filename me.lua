local SAFE = {
  "x",
  "~",
  "~"
}
local CLUSTERS = {
  COUNT = 1,
  STEP = {
    0,
    0,
    0
  },
  START = {
    "x",
    "y",
    "z"
  }
}
local SECTORS = {
  COUNT = 6,
  STEP = {
    0,
    1,
    0
  },
  START = {
    "x",
    "y",
    "z"
  }
}
local BLOCKS = {
  COUNT = 7,
  STEP = {
    -2,
    0,
    0
  },
  START = {
    "x",
    "y",
    "z"
  }
}
local DIRS = {
  "f",
  "f"
}
local drone = component.drone
local ic = component.inventory_controller
me = {
  PIS = {
    0, --CLUSTER
    0, --SECTOR
    0 --BLOCK
  }
}
local rgt = Goto
function Goto(...)
  me.PIS = {0,0,0}
  return rgt(...)
end
local function checkInBounds(Cluster, Sector, Block)
  local TC, TS, TB = CLUSTERS.COUNT, SECTORS.COUNT, BLOCKS.COUNT
  if Cluster>TC or Sector>TS or Block>TB then
    return false, "out of bounds"
  end
  return true
end
function me.GotoSafe()
  Goto(table.unpack(SAFE))
end
function me.ScanChest(side)
  local chest = {}
  for slot = 1, ic.getInventorySize(side) do
    local sdata = ic.getStackInSlot(side, slot)
    if sdata then
      if not chest[sdata.label.."~"..sdata.name] then chest[sdata.label.."~"..sdata.name] = 0 end
      chest[sdata.label.."~"..sdata.name] = chest[sdata.label.."~"..sdata.name] + sdata.size
    end
  end
  return chest
end
function me.Move(Cluster, Sector, Block)
  if not checkInBounds(Cluster, Sector, Block) then return false, "out of bounds" end
  local pos = {}
  if me.PIS[1]~=Cluster then
    pos = {}
    for i = 1, 3 do
      if CLUSTERS.START[i]~="~" then
        table.insert(pos, i, CLUSTERS.START[i]+CLUSTERS.STEP[i]*(Cluster-1))
      else
        table.insert(pos, i, "~")
      end
    end
    Goto(table.unpack(pos))
  end
  if me.PIS[2]~=Sector then
    pos = {}
    for i = 1, 3 do
      if SECTORS.START[i]~="~" then
        table.insert(pos, i, SECTORS.START[i]+SECTORS.STEP[i]*(Sector-1))
      else
        table.insert(pos, i, "~")
      end
    end
    Goto(table.unpack(pos))
  end
  if me.PIS[3]~=Block then
    pos = {}
    for i = 1, 3 do
      if BLOCKS.START[i]~="~" then
        table.insert(pos, i, BLOCKS.START[i]+BLOCKS.STEP[i]*(Block-1))
      else
        table.insert(pos, i, "~")
      end
    end
    Goto(table.unpack(pos))
  end
  me.PIS = {Cluster, Sector, Block}
end
function me.ReindexStore(ClusterBound, SectorBound, BlockBound)
  if not ClusterBound then ClusterBound = CLUSTERS.COUNT end
  if not SectorBound then SectorBound = SECTORS.COUNT end
  if not BlockBound then BlockBound = BLOCKS.COUNT end
  for CurCluster = 1, ClusterBound do
    for CurSector = 1, (CurCluster==ClusterBound and SectorBound or SECTORS.COUNT) do
      for CurBlock = 1, ((CurCluster==ClusterBound and CurSector==SectorBound) and BlockBound or BLOCKS.COUNT) do
        me.Move(CurCluster, CurSector, CurBlock)
        for _, side in ipairs(DIRS) do
          Send("__reindex",{CurCluster..":"..CurSector..":"..CurBlock..":"..side,me.ScanChest(SIDES[side])})
        end
      end
    end
  end
  Send("__reindex_finished")
  me.GotoSafe()
  GotoHome()
end
function me.Sort()
  local LC, LS, LB, C
  if not Pickup() then return true end
  repeat
    for CurCluster = 1, CLUSTERS.COUNT do
      LC = CurCluster
      for CurSector = 1, SECTORS.COUNT do
        LS = CurSector
        for CurBlock = 1, BLOCKS.COUNT do
          LB = CurBlock
          me.Move(CurCluster, CurSector, CurBlock)
          for _, side in ipairs(DIRS) do
            C = 0
            for slot = 1, drone.inventorySize() do
              drone.select(slot)
              drone.drop(SIDES[side])
              C = C + drone.count()
            end
            if C<1 then
              me.GotoSafe()
              GotoHome()
              if not Pickup(true) then
                GotoHome()
                goto ReIndex
              end
              me.GotoSafe()
              GotoHome()
              goto GBreak
            end
          end
        end
      end
    end
    ::GBreak::
  until false
  ::ReIndex::
  me.ReindexStore(LC, LS, LB)
  return true
end
function me.GetFromChest(Cluster, Sector, Block, Side, Id, Count, notReturn)
  if not checkInBounds(Cluster, Sector, Block) then return false, "out of bounds" end
  me.Move(Cluster, Sector, Block)
  local TP = Count
  local FS = true
  local lPIS
  for slot = 1, ic.getInventorySize(SIDES[Side]) do
    DebugPrint("TP: ", TP, "SLOT:", slot)
    if TP<1 then break end
    local sdata = ic.getStackInSlot(SIDES[Side], slot)
    if sdata then
      if sdata.label.."~"..sdata.name == Id then
        FS = true
        for islot = 1, drone.inventorySize() do
          if drone.count(islot)==0 then
            FS = false
            break
          end
        end
        if FS then
          lPIS = {table.unpack(me.PIS)}
          me.GotoSafe()
          GotoHome()
          Drop()
          me.Move(table.unpack(lPIS))
          FS = true
        end
        if sdata.size >= TP then
          ic.suckFromSlot(SIDES[Side], slot, TP)
          TP = 0
        else
          ic.suckFromSlot(SIDES[Side], slot, sdata.size)
          TP = TP - sdata.size
        end
      end
    end
  end
  if not notReturn then
    me.GotoSafe()
    GotoHome()
    Drop()
  end
end
function me.ProcessRequest(Request)
  if type(Request)=="string" then
    Request = Ser.Do(Request)
  end
  for _, item in ipairs(Request) do
    me.GetFromChest(item.Cluster, item.Sector, item.Block, item.Side, item.Id, item.Count, true)
  end
  me.GotoSafe()
  GotoHome()
  Drop()
end
hook("hit", function()
  math.randomseed(computer.uptime()-math.random(-0xffffff, 0xffffff))
  drone.setLightColor(math.random(0, 0xffffff))
  return false
end)