--[[
  Embedded serialization for drones/microcontrollers/etc
  Serialized data format: index(<1 sym hex type><2 sym hex length><data>)value(<1 sym hex type><4 sym hex length><data>)
]]
local Ser = {}
local StringLen, StringFormat = string.len, string.format
local Types = {
  ["string"] = "0",
  ["0"] = "string",
  tostring = tostring,
  fromstring = tostring,
  ["number"] = "1",
  ["1"] = "number",
  tonumber = tonumber,
  fromnumber = tostring,
  ["boolean"] = "2",
  ["2"] = "boolean",
  toboolean = function(n) return (n == "1" and true or false) end,
  fromboolean = function(n) return (n and "1" or "0") end,
  ["table"] = "3",
  ["3"] = "table"
}
local function DecToHex(n, s) return StringFormat("%0"..s.."x", n) end
local function HexToDec(s) return tonumber("0x" .. s) end
function Ser.Serialize(Table)
  local SerializedTable = ""
  for Key, Value in pairs(Table) do
    local KeyType, ValueType = type(Key), type(Value)
    if Types[KeyType] and Types[ValueType] then
      local Element = ""
      local SerializedKey = Types["from"..KeyType](Key)
      Element = Element .. Types[KeyType] .. DecToHex(StringLen(SerializedKey), 2) .. SerializedKey
      local SerializedValue = Types["from"..ValueType](Value)
      Element = Element .. Types[ValueType] .. DecToHex(StringLen(SerializedValue), 4) .. SerializedValue
      SerializedTable = SerializedTable .. Element
    end
  end
  return SerializedTable
end
Types.fromtable = Ser.Serialize
function Ser.Unserialize(SerializedTable)
  if SerializedTable == "" then return {} end
  local UnserializedTable = {}
  local ElementLength, KeyHeader, KeyType, KeySize, Key, ValueHeader, ValueType, ValueSize, Value
  repeat
    ElementLength = 0
    KeyHeader = SerializedTable:sub(1, 3)
    ElementLength = ElementLength + 3
    KeyType = Types[KeyHeader:sub(1, 1)]
    KeySize = HexToDec(KeyHeader:sub(2, 3))
    Key = SerializedTable:sub(ElementLength + 1, ElementLength + KeySize)
    ElementLength = ElementLength + KeySize
    Key = Types["to" .. KeyType](Key)
    ValueHeader = SerializedTable:sub(ElementLength + 1, ElementLength + 5)
    ElementLength = ElementLength + 5
    ValueType = Types[ValueHeader:sub(1, 1)]
    ValueSize = HexToDec(ValueHeader:sub(2, 5))
    Value = SerializedTable:sub(ElementLength + 1, ElementLength + ValueSize)
    ElementLength = ElementLength + ValueSize
    Value = Types["to" .. ValueType](Value)
    UnserializedTable[Key] = Value
    SerializedTable = SerializedTable:sub(ElementLength + 1)
  until SerializedTable == ""
  return UnserializedTable
end
Types.totable = Ser.Unserialize
function Ser.Do(Data)
  if type(Data) == "string" then
    return Ser.Unserialize(Data)
  else
    return Ser.Serialize(Data)
  end
end
return Ser